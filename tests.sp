
#include <sourcemod>

// force this for the include testing.
#pragma semicolon 1
#pragma newdecls required

#define GC_DEBUG

#include <gamechaos>
#include <gamechaos/arrays>
#include <gamechaos/CLIENT>
#include <gamechaos/debug>
#include <gamechaos/maths>
#include <gamechaos/misc>
#include <gamechaos/strings>
#include <gamechaos/tempents>
#include <gamechaos/tracing>
#include <gamechaos/vectors>

#include <gamechaos>
#include <gamechaos/arrays>
#include <gamechaos/CLIENT>
#include <gamechaos/debug>
#include <gamechaos/maths>
#include <gamechaos/misc>
#include <gamechaos/strings>
#include <gamechaos/tempents>
#include <gamechaos/tracing>
#include <gamechaos/vectors>

public void OnPluginStart()
{
	TestArrays();
	TestClient();
	TestDebug();
	TestMaths();
	TestMisc();
	TestStrings();
	TestTracing();
	TestVectors();
}

void TestArrays()
{
	ArrayList test = new ArrayList(32, 2);
	int array[16];
	
	int result = GCSetArrayArrayIndexOffset(test, 1, array, sizeof array, 20);
	GC_ASSERT(result == 12);
	
	result = GCCopyArrayArrayIndex(test, 1, array, sizeof array, 20);
	GC_ASSERT(result == 12);
}

void TestClient()
{
	
}

void TestDebug()
{
	
}

void TestMaths()
{
	
}

void TestMisc()
{
	
}

void TestStrings()
{
	
}

void TestTracing()
{
	
}

void TestVectors()
{
	
}